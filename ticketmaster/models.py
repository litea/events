import uuid
from django.db import models


class Event(models.Model):
    event_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)

    name = models.CharField(max_length=1000, null=False, blank=False)
    promoter_name = models.CharField(max_length=255, null=True, blank=True)
    description = models.CharField(max_length=10000, null=True, blank=True)

    price_min = models.DecimalField(decimal_places=4, max_digits=15, null=True, blank=True)
    price_max = models.DecimalField(decimal_places=4, max_digits=15, null=True, blank=True)

    url = models.URLField(max_length=2000, null=True, blank=True)

    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True)

    class Meta:
        verbose_name = "event"
        verbose_name_plural = "events"
