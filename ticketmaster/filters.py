import django_filters
from django_filters import filters
from django.db.models import Q
from ticketmaster.models import Event


class EventFilter(django_filters.FilterSet):
    name = filters.CharFilter()
    promoter_name = filters.CharFilter()

    start_date = filters.DateFilter(field_name='start_date', lookup_expr='gte')
    end_date = filters.DateFilter(field_name='end_date', lookup_expr='lte', method='filter_end_date')

    price_min = filters.NumberFilter(field_name='price_min', lookup_expr='gte')
    price_max = filters.NumberFilter(field_name='price_max', lookup_expr='lte')

    class Meta:
        model = Event
        fields = ['start_date', 'end_date', 'name', 'promoter_name', 'price_min', 'price_max']

    def filter_end_date(self, queryset, field_name, value):
        return queryset.filter(
            Q(end_date__lte=value) | Q(end_date__isnull=True)
        )
