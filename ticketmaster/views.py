import django_filters
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework import permissions
from rest_framework import filters as rest_filters

from ticketmaster import serializers
from ticketmaster import models
from ticketmaster import filters


class EventViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = [permissions.AllowAny]
    serializer_class = serializers.EventSerializer
    queryset = models.Event.objects.order_by('created_at')

    filter_backends = [
        rest_filters.OrderingFilter,
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    ordering_fields = ['created_at', 'start_date']
    search_fields = [
        'name',
        'promoter_name',
    ]
    filter_class = filters.EventFilter
