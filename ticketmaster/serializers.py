from rest_framework import serializers
from ticketmaster import models


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = models.Event
