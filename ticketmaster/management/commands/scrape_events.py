import requests
from django.db import transaction
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from ticketmaster.models import Event


class Command(BaseCommand):
    help = 'Scrape events'

    api_key = settings.TICKET_MASTER.get('API_KEY', 'eePh4DjcfI7g2qZLAH0iHUcBEurozHTW')
    pages_count = settings.TICKET_MASTER.get('PAGES_COUNT', 50)

    base_url = 'https://app.ticketmaster.com/discovery/v2/events.json?apikey={api_key}&page={page}'

    def handle(self, *args, **options):
        with transaction.atomic():
            for i in range(int(self.pages_count)):
                resp = requests.get(self.base_url.format(page=i, api_key=self.api_key))

                for event in resp.json()['_embedded']['events']:
                    if not event.get('name'):
                        continue

                    price_min = None
                    price_max = None
                    start_date = None
                    end_date = None

                    if event.get('priceRanges'):
                        price_min = event['priceRanges'][0].get('min')
                        price_max = event['priceRanges'][0].get('max')

                    if event.get('dates'):
                        start_date = event['dates'].get('start', {}).get('localDate')
                        end_date = event['dates'].get('end', {}).get('localDate')

                    Event.objects.create(
                        name=event['name'],
                        promoter_name=event.get('promoter', {}).get('name'),
                        description=event.get('info'),
                        price_min=price_min,
                        price_max=price_max,
                        url=event.get('url'),
                        start_date=start_date,
                        end_date=end_date,
                    )
